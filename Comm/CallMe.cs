﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Comm
{
    public class CallMe
    {
        public string GetStr()
        {
            var c = new ServiceReference1.Service1Client();

            // Set client certificate
            c.ClientCredentials.ClientCertificate.SetCertificate(
                StoreLocation.LocalMachine,
               StoreName.My,
               X509FindType.FindBySubjectName,
               "ClientCert"
               );

            // call "ProtectionLevel.EncryptAndSign" method
            var result = c.SayHi();

            return result;
        }
    }
}
